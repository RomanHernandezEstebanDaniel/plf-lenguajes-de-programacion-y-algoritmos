# Programación Lógica y Funcional -SCC1019-ISA-20213

## Actividad lenguajes de programación y algoritmos

### Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)

```plantuml
@startwbs
*[#c894ed] Programando ordenadores en los 80's y ahora. ¿Qué ha cambiado?
**_ en los
***[#f7a3db] Sistemas actuales
****_< sus características:
*****[#SkyBlue]< Sistemas que podemos encontrar en nuestras casas.
*****[#SkyBlue]< Sistemas que puedes comprar actualmente.
*****[#SkyBlue]< La evolución del hardware es muy acelerada.
*****[#SkyBlue]< El software se vuelve lento.
*****[#SkyBlue]< El numero de componentes que se puede meter en un circuito se duplica cada 18 meses.
****_ Diferencias con los sistemas antiguos:
*****[#SkyBlue] Ordenadores construidos a partir del año 2000 en adelante.
*****[#SkyBlue] La continuidad de estos sistemas o ordenadores aun sigue viguente.
*****[#SkyBlue] La potencia de de los sistemas actuales es mucho mayor.
*****[#SkyBlue] Medida de frecuencia utilizada gigahercio (GHz).
***[#f7a3db] Sistemas antiguos
****_< sus características:
*****[#SkyBlue]< Sistemas que ya no estan la venta.
*****[#SkyBlue]< Sistemas que ya no hayan tenido una continuidad.
*****[#SkyBlue] Mejoras sin un cambio significativo.
*****[#SkyBlue] Precios sumamente elevados.
*****[#SkyBlue] El hardware grafico no daba mas
*****[#SkyBlue] El cuello de botella estaba en el hardware.
*****[#SkyBlue]< La evolución del hardware era muy lenta.
*****[#SkyBlue]< El programador tenía que solucionar los cuellos de botella.
****_ Diferencias con los sistemas actuales:

*****[#SkyBlue] Ordenadores de los años 80's y 90's.
*****[#SkyBlue] La potencia de los sistemas antiguos es obsoleta comparada con los sistemas actuales.
*****[#SkyBlue] Medida de frecuencia utilizada megahercio (MHz).

**_ su
***[#f7a3db] Programación
****_ en:
*****[#SkyBlue]< Los años 80's y 90's
******_< características:
*******[#bbe6f6]< Las maquinas no eran potentes, no habia capacidad tecnológica .
*******[#bbe6f6]< Se tenia que conocer a fondo la arquitectura de cada maquina.
*******[#bbe6f6]< Uso de lenguaje de bajo nivel ensamblador.
*******[#bbe6f6]< Se priorizaba la eficiencia.
*******[#bbe6f6]< Limitación de recursos.
*******[#bbe6f6]< Complejidad en el codigo útil.
*****[#SkyBlue] La actualiadad
******_ características:
*******[#bbe6f6] Si tienes una idea de como programar una maquina, puedes programar otra.
*******[#bbe6f6] Uso de lenguaje de bajo alto nivel.
*******[#bbe6f6] Utilización de compiladores e interpretes.
*******[#bbe6f6] Desarrollo con nuevas abstracciones para software mas complejo.
*******[#bbe6f6] Sobrecarga en tiempo de ejecución.
*******[#bbe6f6] El programador no necesita conocimiento de su hardware.
*******[#bbe6f6] Complejidad en la sobrecarga de secuencia de llamadas.

**_ sus
***[#f7a3db] Bugs
****_ es:
*****[#SkyBlue] Un error de software, error o simplemente fallo es un problema en un programa de computador o sistema de software.
****_ en:
*****[#SkyBlue] Los años 80's y 90's
******_ como eran:
*******[#bbe6f6] El producto ya sea programas, videojuegos o utilidades ya eran productos finalizados.
*******[#bbe6f6] Era raro poder encontrarse con un bug.
*******[#bbe6f6] No existia la posibilidad de parchear un bug. 
******_ características en su programación:
*******[#bbe6f6] Se programaba directaba al hardware.
*******[#bbe6f6] Si hubiese un bug la posibilidad del fallo es por parte del programador.
*****[#SkyBlue] La actualiadad
******_< como son:
*******[#bbe6f6]< Hay demasiados bugs encontrados en software de hoy en dia. 
*******[#bbe6f6]< Como las aplicaciones se van actualizando dia a dia aparece nuevos bugs.
*******[#bbe6f6]< Parchear un bug es dar una solución parcial.
*******[#bbe6f6]< Genera mucha ineficiencia.
******_< caracteristicas en su programación:
*******[#bbe6f6]< No se programaba directaba al hardware, se utiliza un lenguaje de alto nivel.
*******[#bbe6f6]< Si hubiese un bug la posibilidad del fallo puede ser intermedio y no por parte del programador.


@endwbs
```
Referencia:
[Programando ordenadores en los 80 y ahora. ¿Qué ha cambiado? (2016)](https://canal.uned.es/video/5a6f4bdcb1111f082a8b4619)

### Hª de los algoritmos y de los lenguajes de programación (2010)

```plantuml
@startwbs
*[#86b6c9] Historia de los algoritmos y de los lenguajes de programación
**[#aca3ce] Algoritmos 
***_ son:
****[#BEEAC2] Conjuntos de instrucciones o reglas definidas y no-ambiguas, ordenadas y finitas.
***_< sirven para:
****[#BEEAC2]< Solucionar un problema.
****[#BEEAC2]< Realizar un computo.
****[#BEEAC2]< Procesar datos.
****[#BEEAC2]< Llevar a cabo otras tareas o actividades.
***_ sus caractersítica:
****[#BEEAC2] Muestra la manera de como llevar acabo procesos.
****[#BEEAC2] Reciben una entrada y la transforman en una salida.
****[#BEEAC2] Debe ser una secuencia ordena y finita de instrucciones no ambiguas.
***_< su historia:
****[#BEEAC2]< Desde la antigua mesopotamia de hace 3000 años se emplearon algoritmos para describir ciertos calculos con comercio.
****[#BEEAC2]< En el siglo XVII aparecieron las primeras ayudas mecánicas en forma de caluladoras de sobremesa.
****[#BEEAC2]< Siglo XIX nacen las primeras maquinas programables.
****[#BEEAC2]< A mediados del siglo XX los algoritmos alcanzan un desarrollo sin precedentes.
****[#BEEAC2]< No surgen de los computadoras.
****[#BEEAC2]< En Babilonia aparecieron tablillas de arcilla de muchos años para calcular la capital.
***_ limites:
****[#BEEAC2] En los años 30's se demostro matematicamente que hay problemas que no admiten una solución por un algoritmo.
****[#BEEAC2] Teoremas matemáticos.
****[#BEEAC2] Nunca sera capaz de resolver problemas indecidibles.
****[#BEEAC2] Solo puede hacer operaciones mecánicas.
****[#BEEAC2] No puedeje ejecutarse asta que sea implementado.
****_ ya sea en:
*****[#BEEAC2] Un lenguaje de programación.
*****[#BEEAC2] Un circuito eléctrico
*****[#BEEAC2] Ua aparato mecánico usando lapiz y papel.
*****[#BEEAC2] Algún otro modelo de computación.
***_< se dividen en:
****[#BEEAC2]< Razonables o polinomiables:
*****_< son:
******[#b3cce8]< Algoritmos cuyo tiempo de ejecución crece despacio a medida que los problemas se hacen mas grandes.
****[#BEEAC2]< No razonables o exponenciales o superpolinomiables:
*****_< son:
******[#b3cce8]< Los que se comportan mal ya que si añadimos un dato mas al problema hace que se duplique el tiempo, crece muy deprisa.

**[#aca3ce] Lenguajes de programación
***_< son:
****[#BEEAC2]< Los instrumentos apropiados para comunicar los algoritmos a las máquinas.  
***_< sus perpectivas son:
****[#BEEAC2]< Sintaxis.
****[#BEEAC2]< Semántica.
****[#BEEAC2]< Pragmática.
***_< se utilizan para:
****[#BEEAC2]< Crear sistemas operativos.
****[#BEEAC2]< Programas de escritorio.
****[#BEEAC2]< Aplicaciones móviles.
****[#BEEAC2]< Resolver problemas o interpretar datos.
***[#BEEAC2] Paradigmas
****_ se dividen en:
*****[#b3cce8] Imperativos.
******_ algunos lenguajes como:
*******[#f6c6d1] Pascal.
*******[#f6c6d1] COBOL.
*******[#f6c6d1] FORTRAN.
*******[#f6c6d1] C.
*******[#f6c6d1] C++.
******[#f6c6d1] Contienen secuencia de instrucciones 
******_ otros enfoques como:
*******[#FADAE2] Programación estructurada.
*******[#FADAE2] Programación procedimental.
*******[#FADAE2] Programación modular.
*****[#b3cce8]< Declarativos.
******[#f6c6d1] Describe el problema en lugar de encontrar una solución al mismo.
******_ se divide en:
*******[#f6c6d1] Programación lógica
*******[#f6c6d1] Programación funcional
*****[#b3cce8]< Programación orientada a objetos.
******_ contiene conceptos como:
*******[#f6c6d1] Abstracción de datos.
*******[#f6c6d1] Encapsulación.
*******[#f6c6d1] Eventos.
*******[#f6c6d1] Modularidad.
*******[#f6c6d1] Herencia.
*******[#f6c6d1] Polimorfismo.

@endwbs
```
Referencia:
[Hª de los algoritmos y de los lenguajes de programación (2010)](https://canal.uned.es/video/5a6f4c8fb1111f082a8b4b9c)

### Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos. La evolución de los lenguajes y paradigmas de programación (2005)

```plantuml
@startwbs
*[#bfbfbf] Paradigmas de programación 
**[#C9FFC2] Evolución de los lenguajes 
***_ surgen para:
****[#AFE4FE] Paliar el salto semántico entre el humano y un ordenador.
****[#AFE4FE] Evitar programar directamente con codigos numéricos.
***[#AFE4FE] Antes:
****_ en un inicio:
****[#AFE4FE] Eran conoconcidos como codigos 
****[#AFE4FE] Lenguaje ensamblador
****_ sirve para:
*****[#C4B5FF] Programar perifericos.
*****[#C4B5FF] Dispositivos a bajo nivel.
*****[#C4B5FF] Realizar algoritmos muy eficientes o rápidos.
***[#AFE4FE] Actualidad: 
****_ surgen:
*****[#C4B5FF] Nuevas tendencias:
****** Nuevos paradigmas a travéz de aspectos.
*****[#C4B5FF] Nuevas abstracciones:
****** Asume el nivel de granularidad.
****** Enfoque en la forma de pensar humana.

**[#C9FFC2] Paradigmas de programación
***[#AFE4FE]< Paradigma 
****_ es:
*****[#C4B5FF] La forma de aproximación a la solución de un problema.
*****[#C4B5FF] La manera de en que nosotros podemos concebir la programacion para dar una solución concreta.
*****[#C4B5FF] La forma de pensar o abordar el desarrollo de un programa.
****[#C4B5FF]< Paradigma funcional
*****_ se basa en:
******[#F6C5E7] Usar el lenguaje de las matemáticas. 
******[#F6C5E7] Funciones matemáticas.
*****[#F6C5E7] Uso de recursividad
*****_< lenguajes importantes:
******[#F6C5E7]< ML
*******_< Cadenas de caracterés
********< - "Hola mundo"; val it= "Hola mundo": string
*******_< Operando enteros
********< - 3+4; it=7: int
*******_< Creación de funciones
********< - fun<nombre><parámetro> = <expresion>; fun doble x = 2*x; fun incremento x=x+1;
******[#F6C5E7]< Clojure
*******_< Factorial recursivo
********< (defn fact [n] (if (> n 1). (* n (fact (dec n))). 1)).
*******_< Factorial con loop
********< (defn fact [n] (loop [current 2 accum 1] (if (< current n). (recur (inc current) (* accum current)). (* accum current)))). 
*******_< Un generador de números únicos y consecutivos que soporta llamadas concurrentes
********< (let [i (atom 0)] (defn generar-id-unica "Devuelve un identificador numérico distinto para cada llamada." [] (swap! i inc))).
******[#F6C5E7]< Scala
*******_< Variables
********< val o val VariableName : DataType = [Initial Value]
*******_< Hola mundo
********< object HelloWorld {def main(args: Array[String]) {println("Hello, world!")}}
*******_< Cadenas de caracterés
********< - "Hola mundo"; val it= "Hola mundo": string
****[#C4B5FF]< Paradigma Logico
*****[#F6C5E7] Uso de recursividad
*****_< se basa en:
******[#F6C5E7]< Expresiones lógicas
******[#F6C5E7] Lenguaje de la lógica.
******[#F6C5E7] Predicaco logico.
*******_ es:
******** Una sentencia que solo puede devolver cierto o falso.
******[#F6C5E7]< Modelizar problemas.
*******_< usa:
********< Mecanismos de inferencia para dar una solución.
*****_ lenguajes por excelencia: 
******[#F6C5E7] PROLOG
*******_ ciclo REPL (Read-Eval-Print Loop)
********[#F6C5E7] %% % padre(X,Y): X es padre de Y padre(luis,ana); padre(juan,luis); buelo(X,Y) :-
*******_ contiene:
********[#F6C5E7] Multiples implementaciones.
****_ Como:
*****[#C4B5FF] Programación estructurada
******_< lenguajes que soportan este tipo:
*******[#F6C5E7]< Basic.
********_< Arreglos:
*********< DIM MatrizDeEnteros(100,100) AS INTEGER; DIM VectorDeEnteros%(30); DIM ListaDeNombres(50) AS STRING
********_< Hola mundo
*********< PRINT "¡Hola Mundo!"
*******[#F6C5E7]< C.
********_< tipos de datos basicos:
*********< Números enteros definidos con la palabra clave int
*********< Letras o caracteres definidos con la palabra clave char
*********< Números reales o en coma flotante definidos con las palabras claves float o double
********_< Ejemplo:
*********< void main(){int i;long int j;long k;short int l;short m;}
*******[#F6C5E7]< Pascal.
********_ tipos de datos básicos:
********* integer (entero)
********* real
********* char (un byte, un carácter)
********* boolean (lógico: verdadero -true- o falso -false-) 
********* DIM
********_ ejemplos:
********* variable_nombre : string; variable_nombre : string[longitud];
*******[#F6C5E7]< Fortran.
********_< tipos de datos básicos:
*********< integer (entero)
*********< real
*********< COMPLEX: Son estructuras como si fuera un par de numero, de esta manera → (a,b) = a + bi
*********< char (un byte, un carácter)
*********< boolean (lógico: verdadero -true- o falso -false-) 
********_< ejemplos:
*********_< variables
**********< implicit none integer i, j, k, ... real*8 x, y, z, ...
*********_< constantes
**********< parameter (cte1=0.7654, pi=3.1415927, ...)
*********_< vectores
**********< dimension x(0:1000), y(0:1000), z(0:1000)
******_ se separa de:
*******[#F6C5E7] Las dependencias de hardware
*******[#F6C5E7] Las estructuras de memoria.
******_ piensa de forma:
*******[#F6C5E7]  Alto nivel
******** Estructuras de datos abstractas.
******** Estrcturas de control 
*********_ permiten:
********** Controlar el flujo del programa.
*******_ ofrece:
********[#F6C5E7] Una concepción al diseño de software mas modular.

***_< surgen por:
****[#AFE4FE]< Crisis en la programación al desarrollar grandes desarrollos de software complejos.
****[#AFE4FE]< La forma de pensar no es el mas adecuado.
****[#AFE4FE]< Fallos dificiles de corregir.


@endwbs
```
Referencia:
[Tendencias actuales en los lenguajes de Programación y Sistemas Informáticos](https://canal.uned.es/video/5a6f4d3cb1111f082a8b4fbc)




Autor:
[RomanHernandezEstebanDaniel @ GitLab](https://gitlab.com/RomanHernandezEstebanDaniel)
